import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { Network } from '@ionic-native/network';
import { MovieProvider } from '../providers/movie/movie';

import { Movie } from '../models/movie.model';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  rootPage:any;
  

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public preferProv: PreferencesProvider,
    public network: Network,
    public movieProv: MovieProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.preferProv.get().then((user) => {
        if (user) {
          this.preferProv.user = user;
          console.log('if',user);
          //existe usúario gravado do preference então redireciona para ListPage
          if(network.type != 'none'){
            this.movieProv.connection = true;
            this.saveDataOfflineIfExists();
          } else {
            this.movieProv.connection = false;
          }
          this.rootPage = 'HomePage';
          //toda vez que o app for aberto atualiza o devicepreference e o array de mobile
          this.hideSplashscreen();
        } else {
          console.log('else',user)
          this.rootPage = 'SingInPage';
          this.hideSplashscreen();
        }
      })

      this.subscribeNetworkConnection();
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  private hideSplashscreen(): void {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 500);
    }
  }

  private saveDataOfflineIfExists(){
    this.movieProv.getAll().then((movies: Movie[]) => {
      if(movies.length > 0){
        movies.forEach(element => {
          if(element.status == 0){
            console.log('gravando cadastros feitos offline no firebase');
            this.movieProv.createFirebase(element).then((uid) => {
              element.status = 1;
              element.uid = uid;
              this.movieProv.update(element)
            }).catch((err: Error) => {
              console.log('erro ao inserir dados gravador offline no banco', err)
            })
          }
        });
      } else {
        console.log('Sem registros offline para gravar no firebase');
      }
    });
  }

  subscribeNetworkConnection(){
    // watch network for a disconnect
    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.movieProv.connection = false;
    });

    // watch network for a connection
    this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        this.movieProv.connection = true;
        this.saveDataOfflineIfExists();          
      }, 3000);
    });
  }

}

