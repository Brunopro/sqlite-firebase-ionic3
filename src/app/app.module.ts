import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite } from '@ionic-native/sqlite';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SqliteHelperProvider } from '../providers/sqlite-helper/sqlite-helper';
import { MovieProvider } from '../providers/movie/movie';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AuthProvider } from '../providers/auth/auth';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { ValidateUserProvider } from '../providers/validate-user/validate-user';
import { IonicStorageModule } from '@ionic/storage';

import { Network } from '@ionic-native/network';

const firebaseConfig = {
  apiKey: "AIzaSyAziH-BlIVlsvoasLHPXARrG9poK142Fg4",
  authDomain: "sqlite-firebase-ionic3.firebaseapp.com",
  databaseURL: "https://sqlite-firebase-ionic3.firebaseio.com",
  projectId: "sqlite-firebase-ionic3",
  storageBucket: "",
  messagingSenderId: "699859793961",
  appId: "1:699859793961:web:8b154912ff46de87e84438"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      preloadModules: true
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    SqliteHelperProvider,
    MovieProvider,
    AngularFireAuth,
    AuthProvider,
    PreferencesProvider,
    ValidateUserProvider,
    Network
  ]
})
export class AppModule {}
