export interface User{
    uid?: string;
    name?: string;
    lastname?: string,
    admin?:boolean,
    phone?: string,
    cpf?: string,
    email: string;
    password?: string;
    password_confirmation?:string,
  }