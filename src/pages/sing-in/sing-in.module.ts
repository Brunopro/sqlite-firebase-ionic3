import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingInPage } from './sing-in';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    SingInPage,
  ],
  imports: [
    IonicPageModule.forChild(SingInPage),
    BrMaskerModule
  ],
})
export class SingInPageModule {}
