import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-sing-in',
  templateUrl: 'sing-in.html',
})
export class SingInPage {

  signinForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private auth: AuthProvider,
    private preferences: PreferencesProvider,
    private formBuilder: FormBuilder
  )
  {          
    this.signinForm = this.formBuilder.group({
      cpf: ['', [ Validators.required, Validators.minLength(14) ]],
      password: ['', [ Validators.required, Validators.minLength(6) ]]
    });
  }

  onSubmit(): void {
    let loading: Loading = this.showLoading();

    this.auth.signInWithEmail(this.signinForm.value)
      .then((data) => {
        
        let user = {
          uid: data["user"]["uid"],
          email: data["user"]["email"]
        }
        console.log('signInWithEmail let user', user);
        this.preferences.create(user);
        loading.dismiss();
        this.navCtrl.setRoot('HomePage');
      }).catch(err => {
        loading.dismiss();
        this.presentAlert('Por favor, verifique as suas credenciais!')
      })
  }


  gotoRegisterPage() {
    this.navCtrl.push('SingUpPage')
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde um momento...'
    });

    loading.present();

    return loading;
  }

  private presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Ocorreu um erro',
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }


}
