import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, ItemSliding, AlertOptions, Loading, Platform } from 'ionic-angular';

import { Movie } from '../../models/movie.model';
import { MovieProvider } from '../../providers/movie/movie';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { AngularFireDatabase } from 'angularfire2/database';
import { User } from '../../models/user.model';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  movies: Movie[] = [];

  user;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public movieProv: MovieProvider,
    public platform: Platform,
    public preferProv: PreferencesProvider,
    public fbDatabase: AngularFireDatabase ) {
      
  }

  ionViewDidLoad() {
    
    this.platform.ready().then(() => {
      this.preferProv.get().then((user) => {
        if (user) {
          if(this.movieProv.connection){
            this.fbDatabase.database.ref(`users/${user.uid}/tasks`).once('value')
            .then((snapshot) => {
              const values = Object.keys(snapshot.val()).map(key => snapshot.val()[key])

              this.movieProv.getAll().then((movies: Movie[]) => {       
                if(movies.length == 0){                           
                  values.forEach(element => {
                    this.movieProv.create(element).then((movie) => {
                      this.movies.push(movie);
                    })
                  });
                } else {
                  if(movies.length >= values.length){
                    this.movies = movies;
                  } else if(movies.length < values.length) {
                    values.forEach(value => {
                      movies.forEach(movie => {
                        if(value.uid == movie.uid){

                        } else {
                          this.movieProv.create(value).then((movie) => {
                            this.movies.push(movie);
                          })
                        }
                      });
                    });
                  }
                }
              })

              //if (this.platform.is('cordova') == false)
            }).catch((err) => {
              console.log('Error ao recuperar tasks do firebasse', err)
              this.movieProv.getAll().then((movies: Movie[]) => {
                this.movies = movies;
              });
            })
          } else {
            console.log('Sem conexão com a internet')
            this.movieProv.getAll().then((movies: Movie[]) => {
              this.movies = movies;
            });
          }

          
        } else {
          console.log('else user home.ts')
        }
      })
      
    });

  }
  




   onSave(type: string, item? :ItemSliding, movie?:Movie): void{
    let title: string = type.charAt(0).toUpperCase() + type.substr(1);
    this.showAlert({
      itemSliding: item,
      title: `${title} movie`,
      type: type,
      movie: movie
    });
  }


  private showAlert(options: { itemSliding?: ItemSliding, title: string, type: string, movie?: Movie }): void {
    let alertOptions: AlertOptions = {
      title: options.title,
      inputs: [
        {
          name: 'title',
          placeholder: 'Movie title'
        }
      ],
      buttons: [
        'Cancel',
        {
          text: 'Save',
          handler: (data) => {
            let loading: Loading = this.showLoading(`Saving ${data.title} movie...`);

            let contextMovie: Movie;

            switch (options.type) {
              case 'create':
                contextMovie = new Movie(data.title);
              break;
              case 'update':
                options.movie.title = data.title;
                contextMovie = options.movie;
              break;
            }

            if('create'){
              if(this.movieProv.connection){
                this.movieProv.createFirebase(contextMovie).then((uid) => {
                  contextMovie.status = 1;
                  contextMovie.uid = uid;
                  this.setDatainSqlite(options, loading, contextMovie)
                }).catch((err: Error) => {
                  loading.dismiss();
                  console.log(err, err.message)
                  this.alertPresent('Erro', 'Ocorreu um erro ao gravar os dados, por favor verifique sua conexão');
                })
              } else {
                contextMovie.status = 0;
                this.setDatainSqlite(options, loading, contextMovie)
              }
            }          
          }
        }
      ]
    };

    if (options.type === 'update'){
      alertOptions.inputs[0]['value'] = options.movie.title;
    }

    this.alertCtrl.create(alertOptions).present();
  }

  private setDatainSqlite( options: {type:string, itemSliding?: ItemSliding},loading: Loading, contextMovie: Movie): void {
    this.movieProv[options.type](contextMovie)
      .then((result: any) => {
        console.log('O que me retornar o create ou update', result)
        if(options.type === 'create') {
          this.movies.unshift(result);
        }
        loading.dismiss();
        //fechar o Sliding
        if(options.itemSliding) options.itemSliding.close();
      })
  }

  

  private showLoading(message?: string): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: message || 'Please wait...'
    });
    loading.present();
    return loading;
  }


  onUpdate(movie){
    console.log('onUpdate');
  }

  onDelete(movie: Movie){
    console.log('onDelete');

    this.alertCtrl.create({
      title: `Do you want to delete '${movie.title}' movie`,
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            let loading: Loading = this.showLoading(`Deleting ${movie.title} movie`);

            this.movieProv.delete(movie.id).then((deleted: boolean ) => {
              console.log('return delete', deleted)
              if (deleted) {
                this.movies.splice(this.movies.indexOf(movie), 1)
              }
              loading.dismiss();
            })
          }
        },
        'No'
      ]
    }).present();
  }

  onCreate(){
    console.log('onCreate');
  }

  private alertPresent(title, message): void {
    this.alertCtrl.create({
      title: title,
      message: message,
      buttons: ['Entendi']    
    }).present();
  }

}
