import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingUpPage } from './sing-up';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    SingUpPage,
  ],
  imports: [
    IonicPageModule.forChild(SingUpPage),
    BrMaskerModule
  ],
})
export class SingUpPageModule {}
