import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { User } from "../../models/user.model";

@Injectable()
export class PreferencesProvider {

  user: User;

  constructor(
    public storage: Storage
  ) {
   if(this.user == undefined){
     console.log('get prefer')
     this.get().then((user) => {
       this.user = user;
       console.log('user', user)
     })
   }
  }

  create(user): Promise<any>  {
    this.user = user;
    return this.storage.set('user', user);
    
  }

  //Vai trazer o user com os dados setados
  get(): Promise<any> {
    return this.storage.ready()
      .then(() => {  
        return this.storage.get('user')
      })

      
  }

  // Quando deslogar deve remover do storage
  //usar o preference para deslogar
  remove(): Promise<boolean> {
    this.user = null
    return this.storage.remove('user')
    .then(() => {
      return true
    })
  }


}
