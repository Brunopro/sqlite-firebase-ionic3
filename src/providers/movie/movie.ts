import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../sqlite-helper/sqlite-helper';
import { Movie } from '../../models/movie.model';
import { Platform } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { PreferencesProvider } from '../preferences/preferences';


@Injectable()
export class MovieProvider {

  public connection: boolean = true;
  private db: SQLiteObject;
  private isFirstCall: boolean = true;

  constructor(
    public sqliteHelperProv: SqliteHelperProvider,
    public platform: Platform,
    public fbDatabase: AngularFireDatabase,
    public preferProv: PreferencesProvider

  ) {
    console.log(`isFirstCall? ${this.isFirstCall}`);
  }

  private getDB(): Promise<SQLiteObject> {
    if (this.isFirstCall) {
      this.isFirstCall = false;
      return this.sqliteHelperProv.getDb('dynamicbox.db')
      .then((db: SQLiteObject) => {
        this.db = db;
        

        console.log('Criando novamente tabela movie', db)

        this.db.executeSql(
          'CREATE TABLE IF NOT EXISTS movie (id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT, status INTEGER)', []
        ).then(success => console.log('Movie table created successfully', success))
          .catch(err => console.error('Error creating movie table!', err));


        return this.db;
      })
    }

    return this.sqliteHelperProv.getDb();
  }

  getAll(orderBy?:string): Promise<Movie[]> {
    return this.getDB()
      .then((db: SQLiteObject) => {

        console.log('db', db)

        return this.db.executeSql('select * from movie', [])
          .then((resultSet) => {
            

            let list: Movie[] = [];

            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }

            console.log('Success getAll movies resultSet', list);

            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      });
  } 

  create(movie: Movie): Promise<Movie> {
    return this.db.executeSql('INSERT INTO movie (title, status) VALUES (?,?)', [movie.title, movie.status]).then(resultSet => {
      movie.id = resultSet.insertId;
      return movie;

    })
    .catch((err: Error) => {
      console.error(`Error creating '${movie.title}' movie!`, err);
      return Promise.reject(err.message || err);
    });
  }

  update(movie: Movie): Promise<boolean> {
    return this.db.executeSql('update movie set title=?, status=? where id=?', [movie.title, movie.status, movie.id,])
            .then(resultSet => resultSet.rowsAffected >= 0)
            .catch((err: Error) => {
              console.error(`Error updating ${movie.title} movie!`, err);
              return Promise.reject(err.message || err);
    });
  }

  delete(id:number): Promise<boolean> {
    return this.db.executeSql('delete from movie where id=?', [id])
            .then(resultSet => resultSet.rowsAffected > 0)
            .catch((err: Error) => {
              console.error(`Error deleting movie with id=${id}`);
              return Promise.reject(err.message || err);
    });
  }

  getById(id:number): Promise<Movie> {
    return this.db.executeSql('select * from movie where id=?', [id])
            .then(resultSet => 
              resultSet.rows.item(0)
            ).catch(( err: Error) => {
              console.log(`Error fetching movie with id=${id}`, err)
            });
  }


  createFirebase(movie: Movie):Promise<any>{

    return new Promise((resolve, reject) => {
        let newMobileRef  = this.fbDatabase.database.ref(`users/${this.preferProv.user.uid}/tasks`).push();
  
        newMobileRef.set({
          uid: newMobileRef.key,
          title: movie.title
        }).then(() => {
          console.log('resolve', newMobileRef.key)
          resolve(newMobileRef.key);
        }).catch((err) => {
          console.log('reject', err);
          reject(err);
        })
    });

  }



}
