import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../models/user.model';


@Injectable()
export class AuthProvider {

  constructor(
    public db: AngularFireDatabase,
    public af: AngularFireAuth
  ) {
    //code here..
  }



  signInWithEmail(credentials) {
    return this.getEmailByCPF(credentials.cpf).then((email) => {
      return this.af.auth.signInWithEmailAndPassword(email, credentials.password).then((data) => {
        console.log('data', data);
        console.log('datasprig', JSON.stringify(data))
        return data;
      })
    }).catch(err => 
      console.log(err)
    )
  }

  getEmailByCPF(cpf): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.db.database.ref('cpfs').orderByChild('cpf').equalTo(cpf).on("value", function(snapshot){
        if(snapshot.exists()){
          snapshot.forEach(function(data) {
            let email = data.val().email;
            resolve(email)
          })
        } else {
          reject('err')
        }
      })
    })
  }


  signUpWithEmail(user) {
    return this.af.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(() => {
        let uid = this.af.auth.currentUser.uid;
        return this.db.database.ref(`users/${uid}`).set({
          name: user.name,
          email: user.email,
          photo: user.photo ? user.photo : '',
          phone: user.phone ? user.phone : '',
          cpf: user.cpf,
          face_or_google: false,
        }).then(() => {
          //Gravando em um nó de cpfs para fazer login com cpf, e poder resetar senha no forgetPassword
          return this.db.database.ref('cpfs').child(uid).set({
            email: user.email,
            cpf: user.cpf
          }).then((user.uid))
        })
      })
  }


  getUserInDatabase(uid): Promise<any> {

    return new Promise((resolve, reject) => {
      this.db.database.ref('users').child(uid).on('value', (snapshot) => {
        let result = snapshot.val()
        let user: User = {
          uid: uid,
          name: result.name ? result.name : '',
          email: result.email ? result.email : '',
          cpf: result.cpf ? result.cpf : '',
        }
        resolve(user)
        console.log('criado o preference', JSON.stringify(user))
      });
    })
  }




}
